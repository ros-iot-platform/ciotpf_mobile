# ciotpf_mobile

## _ciotpf_mobile_android  
こっちでGUIとか概要を作り込んで、ciotpf_mobile_androidでROS2周りを作り込む。
ソースとかlayoutディレクトリとかはシンボリックリンクしちゃって共有してる。
CiotpfRosNode.javaは共有せずそれぞれ独立で書いて, _ciotpf_mobile_androidのほうではモックのデータを流すようにしている。

なので、Android　Studioからビルドするとモックのデータが見える。
build.shでビルドするとROS2 javaが使えるので本物のデータが見える






# 立ち上げ後かならずこれ動かす
シンボリックリンクで、手元のプロジェクトをros2_android_ws内にリンクする
ln -s /root/workspace/ciotpf_mobile_android/ /root/ros2_android_ws/src
ln -s /root/workspace/ciotpf_messages/ /root/ros2_android_ws/src

adbへのパス通す
echo 'export PATH="$PATH:/opt/android/android-sdk-linux/platform-tools"' >> ~/.bashrc


これが出来たらbuild.sh動かすとビルド出来る

send.shでビルドしたやつをデバイスに送る
  
adb devicesで繋がってるデバイス一覧見れる

ros2_javaの中身の一部ファイルを、workspace/memo以下のファイルと置き換える。  
ホントはros2_javaフォークしてくるべきなんだけど。。。。。
めんどいので一旦機能追加した分はがんばってマニュアル管理



##  android studio install
https://developer.android.com/studio/install?hl=ja
ダウンロードしてきたファイルを/opt/androidに置いて、
tar -xzvf android-studio-ide-201.6953283-linux.tar.gz 
で解凍

/opt/android/android-studio/bin/studio.sh
を実行  
android sdk locationに
/opt/android/android-sdk-linux
を指定


/opt/android/android-studio/bin/studio.sh
これで起動出来るよ！

## gradle4.9 install
###4.9
apt install gradleだとversion4が入れられるけど4.9がほしい

mkdir /opt/gradle
wget https://services.gradle.org/distributions/gradle-4.9-bin.zip -P /tmp
unzip -d /opt/gradle /tmp/gradle-*.zip
echo 'export PATH="$PATH:/opt/gradle/gradle-4.9/bin"' >> ~/.bashrc




## 日本語対応
これいらない！！！！！！！けど一応書いておく。
apt-get install language-pack-ja-base language-pack-ja
locale-gen ja_JP.UTF-8
echo export LANG=ja_JP.UTF-8 >> ~/.profile




# メモ

### layoutのシンボリックリンク
ln -s /root/workspace/_ciotpf_mobile_android/app/src/main/res/layout ./layout
これで、_ciotpf_mobile_androidのレイアウトをそのまま使える

### menuのシンボリックリンク
ln -s /root/workspace/_ciotpf_mobile_android/app/src/main/res/menu /root/workspace/ciotpf_mobile_android/src/main/res/menu



### パーミッション一括変換。これでホストからいじれるよ。（その前にubuntuってユーザーつくってから。）
さっさとデフォルトでroot以外でログイン出来るようにしないと・・・・
chown -R ubuntu ./



### エラーのログだけ
adb logcat *:E

