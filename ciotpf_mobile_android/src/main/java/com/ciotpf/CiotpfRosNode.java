package com.ciotpf;

import org.ros2.rcljava.node.BaseComposableNode;
import org.ros2.rcljava.subscription.Subscription;
import org.ros2.rcljava.publisher.Publisher;
import org.ros2.rcljava.RCLJava;
import org.ros2.rcljava.node.Node;
import org.ros2.rcljava.client.Client;

import org.ros2.rcljava.executors.Executor;
import org.ros2.rcljava.executors.SingleThreadedExecutor;
import org.ros2.rcljava.interfaces.MessageDefinition;

import android.os.Handler;
import android.os.Looper;
import java.util.Timer;
import java.util.TimerTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import com.ciotpf.ciotpfmobile.utils.TopicDataModel;
import com.ciotpf.ciotpfmobile.utils.AppNodeInfoDataModel;
import com.ciotpf.ciotpfmobile.utils.ManualTopicDataModel;
import com.ciotpf.ciotpfmobile.utils.TopicDataModel;

import ciotpf_messages.srv.CiotpfAppNodeManualTopicConfig;

//Singleton
public class CiotpfRosNode {

    private static final CiotpfRosNode INSTANCE = new CiotpfRosNode();
    private static long SPINNER_DELAY = 0;
    private static long SPINNER_PERIOD_MS = 200;


    private Handler mainThreadHandler;
    private Executor rosExecutor;
    private Timer timer;


    public final String nodeName;
    private final BaseComposableNode node;

    private CiotpfRosNode() {
        RCLJava.rclJavaInit();

        this.nodeName = "ciotpf_mobile_app_node";
        this.node = new BaseComposableNode(this.nodeName);

    }

    private Node getNode() {
        return node.getNode();
    }


    public static CiotpfRosNode getInstance() {
        return INSTANCE;
    }

    public void addSubscription() {

    }

    public void removeSubscription() {

    }

    public void removeAllSubscription() {

    }

    public <T extends MessageDefinition> Publisher<T> createPublisher(final Class<T> messageType, final String topicName) {
        return this.getNode().<T>createPublisher(messageType, topicName);
    }

    public <T extends MessageDefinition> void publishTopic(final Class<T> messageType, final T message, final String topicName) {
        Publisher publisher = this.getNode().<T>createPublisher(messageType, topicName);
        publisher.publish(message);
    }


    public String[][] getAllTopics() {
        String[][] result =  this.getNode().getTopicNamesAndTypes();
        Log.e("Ciotpf", String.valueOf(result.length));
        return result;
    }

    // topicName: "/ciotpf/app/*"
    public ArrayList<TopicDataModel> getTopicsByWildCard(String topicName) {
        topicName = topicName.replace("*", ".*");

        String[][] topics =  this.getNode().getTopicNamesAndTypes();

        ArrayList<TopicDataModel> resultList = new ArrayList<TopicDataModel>();
        for(String[] topic: topics) {
            if(topic[0].matches(topicName)) {
                resultList.add(new TopicDataModel(topic));
            }
        }

        return resultList;
    }

    public ArrayList<TopicDataModel> getTopicsByTypeName(String typeName) {

        String[][] allTopic = getAllTopics();

        ArrayList<TopicDataModel> dataModels = new ArrayList<>();
        for(String[] topic: allTopic) {
            
            //typeName can be both std_msgs/msg/Bool   std_msgs/Bool
            //so, just compare last element
            // TODO: FIX THISS!!!!!!!
            String[] _tmp = topic[1].split("/");
            String[] _typeName = typeName.split("/");
            
            String __tmp = _tmp[_tmp.length - 1];
            String __typeName = _typeName[_typeName.length - 1];
            if(__tmp.equals(__typeName)) {
                dataModels.add(new TopicDataModel(topic));
            }
        }

        return dataModels;
    }


    public ArrayList<String> getAllAppName() {

        ArrayList<TopicDataModel> topics = this.getTopicsByWildCard("/ciotpf/app/*");
        ArrayList<String> topicNameList = 
                    topics.stream().map(topic -> topic.topicName.split("/")[3]).distinct().collect(Collectors.toCollection(ArrayList::new));

        return topicNameList;

    }
    
    public AppNodeInfoDataModel getAppInfo(String appName) {
        Log.d("CiotpfRosNode", "start getAppInfo");

        List<ciotpf_messages.msg.CiotpfManualTopic> subscriptions;
        List<ciotpf_messages.msg.CiotpfManualTopic> publishers;

        try {
            Client<ciotpf_messages.srv.CiotpfAppNodeInfo> client =
            this.getNode().<ciotpf_messages.srv.CiotpfAppNodeInfo>createClient(ciotpf_messages.srv.CiotpfAppNodeInfo.class, "/ciotpf/app/"+appName+"/app_node_info");

            ciotpf_messages.srv.CiotpfAppNodeInfo_Request request = new ciotpf_messages.srv.CiotpfAppNodeInfo_Request();

            Future<ciotpf_messages.srv.CiotpfAppNodeInfo_Response> future = client.asyncSendRequest(request);

            //time out 2sec  TODO: NOT WORKING!!!!!!!!!!!!!!!!!!!!
            ciotpf_messages.srv.CiotpfAppNodeInfo_Response response = future.get(2L, TimeUnit.SECONDS);

            subscriptions = response.getSubscriptions();
            publishers = response.getPublishers();

        } catch (Exception e) {
            Log.e("CiotpfRosNode", "Error cannot get app info");
            Log.e("CiotpfRosNode", e.toString());
            return new AppNodeInfoDataModel(new ManualTopicDataModel[] {}, new ManualTopicDataModel[] {});
        } 
        Log.d("CiotpfRosNode", "finish request. start parsing");


        ArrayList<ManualTopicDataModel> _subscriptions = new ArrayList<>();
        ArrayList<ManualTopicDataModel> _publishers = new ArrayList<>();

        for(ciotpf_messages.msg.CiotpfManualTopic topic: subscriptions) {

            ciotpf_messages.msg.CiotpfStringArray tmp = topic.getTopicNames();
            String[] _tmp = tmp.getData().toArray(new String[tmp.getData().size()]);
            ManualTopicDataModel _topic = new ManualTopicDataModel(topic.getName(),topic.getTopicType(),_tmp);

            _subscriptions.add(_topic);
        }
        for(ciotpf_messages.msg.CiotpfManualTopic topic: publishers) {

            ciotpf_messages.msg.CiotpfStringArray tmp = topic.getTopicNames();
            String[] _tmp = tmp.getData().toArray(new String[tmp.getData().size()]);
            ManualTopicDataModel _topic = new ManualTopicDataModel(topic.getName(),topic.getTopicType(),_tmp);
            
            _publishers.add(_topic);

        }

        ManualTopicDataModel[] __subscriptions = _subscriptions.toArray(new ManualTopicDataModel[_subscriptions.size()]);
        ManualTopicDataModel[] __publishers = _publishers.toArray(new ManualTopicDataModel[_publishers.size()]);
        Log.e("CiotpfRosNode", "finish getAppInfo");

        return new AppNodeInfoDataModel(__subscriptions, __publishers);
    }



    //manual sub/pubの設定をする関数。サービスを呼ぶ。
    //isSubscription: subscriptionかpublishか
    //isAdd: 登録するか削除するか
    //topicName: 登録・削除するトピック名
    //name: appの、今回操作したい機能の名前。例：LightsStatus, Lights
    public boolean setManualTopicConfig(String appName, boolean isSubscription, boolean isAdd, String topicName, String name) {
        Log.d("CiotpfRosNode", "start setManualTopicConfig");

        ciotpf_messages.srv.CiotpfAppNodeManualTopicConfig_Request manualTopicConfigRequest = new ciotpf_messages.srv.CiotpfAppNodeManualTopicConfig_Request();
        manualTopicConfigRequest.setIsSubscription(isSubscription);
        manualTopicConfigRequest.setIsAdd(isAdd);
        manualTopicConfigRequest.setTopicName(topicName);
        manualTopicConfigRequest.setName(name);


        ciotpf_messages.srv.CiotpfAppNodeManualTopicConfig_Response response;

        try {

            Log.d("CiotpfRosNode",  "/ciotpf/app/"+appName+"/manual_topic_config");
            Client<ciotpf_messages.srv.CiotpfAppNodeManualTopicConfig> client =
            this.getNode().<ciotpf_messages.srv.CiotpfAppNodeManualTopicConfig>createClient(ciotpf_messages.srv.CiotpfAppNodeManualTopicConfig.class, "/ciotpf/app/"+appName+"/manual_topic_config");
            Log.d("CiotpfRosNode", "start sending request");

            Future<ciotpf_messages.srv.CiotpfAppNodeManualTopicConfig_Response> future = client.asyncSendRequest(manualTopicConfigRequest);

            //time out 2sec  TODO: NOT WORKING!!!!!!!!!!!!!!!!!!!!
            response = future.get(2L, TimeUnit.SECONDS);

        } catch (Exception e) {
            Log.e("CiotpfRosNode", e.toString());
            return false;
        }

        boolean isSuccess = response.getIsSuccess();
        String message = response.getMessage();


        Log.e("Ciotpf", message);

        return isSuccess;
    }




    public void startNode() {
        this.mainThreadHandler = new Handler(Looper.getMainLooper());
        this.rosExecutor = new SingleThreadedExecutor();
        this.rosExecutor.addNode(this.node);

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Runnable runnable = new Runnable() {
                    public void run() {
                        rosExecutor.spinSome();
                    }
                };
                mainThreadHandler.post(runnable);
            }
        }, CiotpfRosNode.SPINNER_DELAY, CiotpfRosNode.SPINNER_PERIOD_MS);

    }

    public void stopNode() {
        this.rosExecutor.removeNode(this.node);
        if (timer != null) {
            timer.cancel();
        }
    }
}
