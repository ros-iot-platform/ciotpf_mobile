package com.ciotpf;

import android.os.Handler;
import android.os.Looper;
import java.util.Timer;
import java.util.TimerTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;

import com.ciotpf.ciotpfmobile.utils.AppNodeInfoDataModel;
import com.ciotpf.ciotpfmobile.utils.ManualTopicDataModel;
import com.ciotpf.ciotpfmobile.utils.TopicDataModel;

import org.ros2.rcljava.interfaces.MessageDefinition;
import org.ros2.rcljava.publisher.Publisher;

//Singleton
public class CiotpfRosNode {

    private static final CiotpfRosNode INSTANCE = new CiotpfRosNode();
    private static long SPINNER_DELAY = 0;
    private static long SPINNER_PERIOD_MS = 200;


    public final String nodeName;

    private CiotpfRosNode() {
        this.nodeName = "ciotpf_mobile_app_node";

    }



    public static CiotpfRosNode getInstance() {
        return INSTANCE;
    }

    public void addSubscription() {

    }

    public void removeSubscription() {

    }

    public void removeAllSubscription() {

    }

    public <T extends MessageDefinition> Publisher<T> createPublisher(final Class<T> messageType, final String topicName) {


        return new Publisher();
    }

    public <T extends MessageDefinition> void publishTopic(final Class<T> messageType, final T message, final String topicName) {

    }


    public String[][] getAllTopics() {
        String[][] result =  {{"topicName", "std_msgs/msg/Bool"}, {"topicName2", "std_msgs/msg/String"}};
        return result;
    }

    // topicName: "/ciotpf/app/*"
    public ArrayList<TopicDataModel> getTopicsByWildCard(String topicName) {
        ArrayList<TopicDataModel> dataModels = new ArrayList<>();
        dataModels.add(new TopicDataModel(new String[]{"abc", "def"}));

        return dataModels;
    }

    public ArrayList<TopicDataModel> getTopicsByTypeName(String typeName) {

        String[][] allTopic = getAllTopics();

        ArrayList<TopicDataModel> dataModels = new ArrayList<>();
        for(String[] topic: allTopic) {

            //typeName can be both std_msgs/msg/Bool   std_msgs/Bool
            //so, just compare last element
            // TODO: FIX THISS!!!!!!!
            String[] _tmp = topic[1].split("/");
            String[] _typeName = typeName.split("/");

            String __tmp = _tmp[_tmp.length - 1];
            String __typeName = _typeName[_typeName.length - 1];
            if(__tmp.equals(__typeName)) {
                dataModels.add(new TopicDataModel(topic));
            }
        }

        return dataModels;
    }

    public ArrayList<String> getAllAppName() {

        ArrayList<String> result = new ArrayList<>();
        result.add("aapname");

        return result;

    }

    public AppNodeInfoDataModel getAppInfo(String appName) {
        ArrayList<String> tmp = new ArrayList<>();
        tmp.add("aaa");
        tmp.toArray();

        ManualTopicDataModel subscription1 = new ManualTopicDataModel("test_sub", "std_msgs/msg/Bool",new String[] {"/sub_topic1", "/sub_topic2"});
        ManualTopicDataModel publisher1 = new ManualTopicDataModel("test_pub", "std_msgs/msg/Bool",new String[] {"/pub_topic1", "/pub_topic2"});


        return new AppNodeInfoDataModel(new ManualTopicDataModel[] {subscription1}, new ManualTopicDataModel[] {publisher1});
    }


    //manual sub/pubの設定をする関数。サービスを呼ぶ。
    //isSubscription: subscriptionかpublishか
    //isAdd: 登録するか削除するか
    //topicName: 登録・削除するトピック名
    //name: appの、今回操作したい機能の名前。例：LightsStatus, Lights
    public boolean setManualTopicConfig(String appName, boolean isSubscription, boolean isAdd, String topicName, String name) {


        return true;
    }




    public void startNode() {

    }

    public void stopNode() {

    }
}


