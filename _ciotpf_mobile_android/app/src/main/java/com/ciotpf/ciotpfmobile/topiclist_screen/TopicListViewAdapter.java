package com.ciotpf.ciotpfmobile.topiclist_screen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.ciotpf.ciotpfmobile.R;
import com.ciotpf.ciotpfmobile.utils.TopicDataModel;

import java.util.List;

public class TopicListViewAdapter extends ArrayAdapter<TopicDataModel> {
    private LayoutInflater layoutInflater;

    public TopicListViewAdapter(Context context, int resource, List<TopicDataModel> objects) {
        super(context, resource, objects);
        this.layoutInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
        );

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(
                    R.layout.topic_list_view_item,
                    parent,
                    false
            );
        }

        TopicDataModel dataModel = (TopicDataModel)getItem(position);

        TextView topicTypeTV = convertView.findViewById(R.id.topic_list_view_item_topictype_textview);
        TextView topicNameTV = convertView.findViewById(R.id.topic_list_view_item_topicname_textview);

        topicNameTV.setText(dataModel.topicName);
        topicTypeTV.setText(dataModel.topicType);


        return convertView;
    }
}
