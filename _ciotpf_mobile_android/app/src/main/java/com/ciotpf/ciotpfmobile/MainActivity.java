package com.ciotpf.ciotpfmobile;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import com.ciotpf.CiotpfRosNode;
import com.ciotpf.ciotpfmobile.topiclist_screen.TopicListActivity;
import com.ciotpf.ciotpfmobile.applist_screen.AppListActivity;

public class MainActivity extends AppCompatActivity implements OnClickListener{

    private Button remoteControllerButton;
    private Button appListButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("main !!");
        setSupportActionBar(toolbar);



        this.remoteControllerButton = (Button)findViewById(R.id.remoteControllerScreenButton);
        this.remoteControllerButton.setOnClickListener(this);

        this.appListButton = (Button)findViewById(R.id.main_activity_app_list_btn);
        this.appListButton.setOnClickListener(this);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.reload).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    private void moveToTopicListScreen() {
        Intent intent = new Intent(this, TopicListActivity.class);
        startActivity(intent);
    }

    private void moveToAppListScreen() {
        Intent intent = new Intent(this, AppListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

        if(v == remoteControllerButton) {
            moveToTopicListScreen();
            return;
        }
        if(v == appListButton) {
            moveToAppListScreen();
            return;
        }
    }
}