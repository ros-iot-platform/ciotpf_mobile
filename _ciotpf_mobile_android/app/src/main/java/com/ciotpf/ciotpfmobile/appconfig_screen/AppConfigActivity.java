package com.ciotpf.ciotpfmobile.appconfig_screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ciotpf.CiotpfRosNode;
import com.ciotpf.ciotpfmobile.R;
import com.ciotpf.ciotpfmobile.appconfig_screen.topicconfig_screen.TopicConfigActivity;
import com.ciotpf.ciotpfmobile.publish_topic_screen.PublishTopicActivity;
import com.ciotpf.ciotpfmobile.utils.AppNodeInfoDataModel;
import com.ciotpf.ciotpfmobile.utils.ManualTopicDataModel;
import com.ciotpf.ciotpfmobile.utils.TopicDataModel;

public class AppConfigActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final String EXTRA_APP_NAME = "extra_app_name";

    private String appName;
    private AppConfigListViewAdapter appConfigListViewAdapter;
    private ListView appConfigListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_config);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("App Config");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle extras = getIntent().getExtras();
        appName = (String) extras.getSerializable(EXTRA_APP_NAME);


        this.appConfigListView = (ListView)findViewById(R.id.app_config_list_view);
        this.appConfigListView.setOnItemClickListener(this);

        AppNodeInfoDataModel appNodeInfoDataModel = new AppNodeInfoDataModel(new ManualTopicDataModel[] {}, new ManualTopicDataModel[] {});
        appConfigListViewAdapter = new AppConfigListViewAdapter(this, appNodeInfoDataModel);
        this.appConfigListView.setAdapter(appConfigListViewAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        reloadData();
    }

    private void reloadData() {
        AppNodeInfoDataModel appNodeInfoDataModel = CiotpfRosNode.getInstance().getAppInfo(appName);

        appConfigListViewAdapter.replaceData(appNodeInfoDataModel);
        appConfigListViewAdapter.notifyDataSetChanged();
    }

    public static void startActivity(Context context, String appName) {
        Intent intent = new Intent(context, AppConfigActivity.class);
        intent.putExtra(EXTRA_APP_NAME, appName);
        context.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
            case R.id.reload:
                reloadData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ManualTopicDataModel dataModel = (ManualTopicDataModel)appConfigListViewAdapter.getItem(position);
        try {
            boolean tmp = appConfigListViewAdapter.isSubscribe(position);
            TopicConfigActivity.startActivity(this, appName, dataModel,tmp);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}