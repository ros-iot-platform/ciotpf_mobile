package com.ciotpf.ciotpfmobile.publish_topic_screen;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ciotpf.ciotpfmobile.R;

import org.w3c.dom.Text;

import com.ciotpf.ciotpfmobile.utils.TopicDataModel;
import com.ciotpf.CiotpfRosNode;

import org.ros2.rcljava.publisher.Publisher;

public class PublishTopicActivity extends AppCompatActivity {

    public static final String EXTRA_TOPIC_DATA_MODEL = "extra_topic_data_model";

    private TextView topicNameTV;
    private Button publishTrueBtn;
    private Button publishFalseBtn;

    private TopicDataModel topicDataModel;

    Publisher pub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        topicDataModel = (TopicDataModel) extras.getSerializable(EXTRA_TOPIC_DATA_MODEL);

        setContentView(R.layout.activity_publish_topic);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Publish Topic");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        topicNameTV = (TextView)findViewById(R.id.publish_topic_topic_name_tv);
        publishTrueBtn = (Button)findViewById(R.id.publish_topic_btn_true);
        publishFalseBtn = (Button)findViewById(R.id.publish_topic_btn_false);

        publishTrueBtn.setOnClickListener(onClickPublisBtn);
        publishFalseBtn.setOnClickListener(onClickPublisBtn);


        if(topicDataModel.topicType.contains("Bool")) {
            topicNameTV.setText(topicDataModel.topicName);
        } else {
            topicNameTV.setText("Bool only!!!!!");
            publishTrueBtn.setEnabled(false);
            publishFalseBtn.setEnabled(false);
            publishTrueBtn.setBackgroundColor(Color.DKGRAY);
            publishFalseBtn.setBackgroundColor(Color.DKGRAY);

        }

        this.pub = CiotpfRosNode.getInstance().<std_msgs.msg.Bool>createPublisher(std_msgs.msg.Bool.class,topicDataModel.topicName);

    }

    public static void startActivity(Context context, TopicDataModel topicDataModel) {
        Intent intent = new Intent(context, PublishTopicActivity.class);
        intent.putExtra(EXTRA_TOPIC_DATA_MODEL, topicDataModel);
        context.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.reload).setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private View.OnClickListener onClickPublisBtn = new View.OnClickListener() {
        public void onClick(final View view) {
            if(view == publishTrueBtn) {

                std_msgs.msg.Bool msg = new std_msgs.msg.Bool();
                msg.setData(true);
                pub.publish(msg);

            }
            if(view == publishFalseBtn) {

                std_msgs.msg.Bool msg = new std_msgs.msg.Bool();
                msg.setData(false);
                pub.publish(msg);

            }
        }
    };
}

/*
package com.ciotpf.ciotpfmobile.publish_topic_screen;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.ciotpf.ciotpfmobile.R;

import org.w3c.dom.Text;

import com.ciotpf.ciotpfmobile.utils.TopicDataModel;
import com.ciotpf.CiotpfRosNode;


import org.ros2.rcljava.publisher.Publisher;

public class PublishTopicActivity extends Activity {

    public static final String EXTRA_TOPIC_DATA_MODEL = "extra_topic_data_model";

    private TextView topicNameTV;
    private Button publishTrueBtn;
    private Button publishFalseBtn;

    private TopicDataModel topicDataModel;

    Publisher pub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        topicDataModel = (TopicDataModel) extras.getSerializable(EXTRA_TOPIC_DATA_MODEL);

        setContentView(R.layout.activity_publish_topic);

        topicNameTV = (TextView)findViewById(R.id.publish_topic_topic_name_tv);
        publishTrueBtn = (Button)findViewById(R.id.publish_topic_btn_true);
        publishFalseBtn = (Button)findViewById(R.id.publish_topic_btn_false);

        publishTrueBtn.setOnClickListener(onClickPublisBtn);
        publishFalseBtn.setOnClickListener(onClickPublisBtn);


        if(topicDataModel.topicType.contains("Bool")) {
            topicNameTV.setText(topicDataModel.topicName);
        } else {
            topicNameTV.setText("Bool only!!!!!");
            publishTrueBtn.setEnabled(false);
            publishFalseBtn.setEnabled(false);
            publishTrueBtn.setBackgroundColor(Color.DKGRAY);
            publishFalseBtn.setBackgroundColor(Color.DKGRAY);

        }

        this.pub = CiotpfRosNode.getInstance().<std_msgs.msg.Bool>createPublisher(std_msgs.msg.Bool.class,topicDataModel.topicName);

    }

    public static void startActivity(Context context, TopicDataModel topicDataModel) {
        Intent intent = new Intent(context, PublishTopicActivity.class);
        intent.putExtra(EXTRA_TOPIC_DATA_MODEL, topicDataModel);
        context.startActivity(intent);
    }


    private View.OnClickListener onClickPublisBtn = new View.OnClickListener() {
        public void onClick(final View view) {
            if(view == publishTrueBtn) {
                std_msgs.msg.Bool msg = new std_msgs.msg.Bool();
                msg.setData(true);
                pub.publish(msg);

            }
            if(view == publishFalseBtn) {
                std_msgs.msg.Bool msg = new std_msgs.msg.Bool();
                msg.setData(false);
                pub.publish(msg);

            }
        }
    };
}
 */