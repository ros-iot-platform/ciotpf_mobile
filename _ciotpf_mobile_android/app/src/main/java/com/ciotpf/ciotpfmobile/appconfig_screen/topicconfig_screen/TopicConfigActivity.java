package com.ciotpf.ciotpfmobile.appconfig_screen.topicconfig_screen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.ciotpf.CiotpfRosNode;
import com.ciotpf.ciotpfmobile.appconfig_screen.AppConfigActivity;
import com.ciotpf.ciotpfmobile.utils.AppNodeInfoDataModel;
import com.ciotpf.ciotpfmobile.utils.ManualTopicDataModel;
import com.ciotpf.ciotpfmobile.utils.TopicDataModel;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ciotpf.ciotpfmobile.R;


import java.util.ArrayList;

public class TopicConfigActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{
    private static final String EXTRA_APP_NAME = "EXTRA_APP_NAME";
    private static final String EXTRA_MANUAL_TOPIC_DATAMODEL = "EXTRA_MANUAL_TOPIC_DATAMODEL";
    private static final String EXTRA_IS_SUBSCRIPTION = "EXTRA_IS_SUBSCRIPTION";


    ManualTopicDataModel manualTopicDataModel;
    boolean isSubscription;
    private String appName;
    private TopicConfigListViewAdapter topicConfigListViewAdapter;

    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_config);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Topic Config");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Bundle extras = getIntent().getExtras();
        manualTopicDataModel = (ManualTopicDataModel) extras.getSerializable(EXTRA_MANUAL_TOPIC_DATAMODEL);
        isSubscription = (boolean) extras.getSerializable(EXTRA_IS_SUBSCRIPTION);
        appName = (String) extras.getSerializable(EXTRA_APP_NAME);

        listView = (ListView)findViewById(R.id.topic_config_list_view);
        listView.setOnItemClickListener(this);


        ArrayList<TopicConfigListViewDataModel> listViewDataModels = new ArrayList<>();
        topicConfigListViewAdapter = new TopicConfigListViewAdapter(this, 0, listViewDataModels);
        listView.setAdapter(topicConfigListViewAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        reloadData();
    }

    public static void startActivity(Context context, String appName, ManualTopicDataModel manualTopicDataModel, boolean isSubscription) {
        Intent intent = new Intent(context, TopicConfigActivity.class);
        intent.putExtra(EXTRA_MANUAL_TOPIC_DATAMODEL, manualTopicDataModel);
        intent.putExtra(EXTRA_IS_SUBSCRIPTION, isSubscription);
        intent.putExtra(EXTRA_APP_NAME, appName);


        context.startActivity(intent);
    }

    private void reloadData() {
        ArrayList<TopicDataModel> topics =  CiotpfRosNode.getInstance().getTopicsByTypeName(manualTopicDataModel.topic_type);

        ArrayList<TopicConfigListViewDataModel> listViewDataModels = new ArrayList<>();
        for(TopicDataModel topic: topics) {
            boolean isAlreadySubscribed = false;

            for(String tp: manualTopicDataModel.topic_names){
                if(tp.equals(topic.topicName)) {
                    isAlreadySubscribed = true;
                }
            }

            TopicConfigListViewDataModel dm = new TopicConfigListViewDataModel(topic.topicName, isAlreadySubscribed);
            listViewDataModels.add(dm);
        }

        topicConfigListViewAdapter.clear();
        topicConfigListViewAdapter.addAll(listViewDataModels);
        topicConfigListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
            case R.id.reload:
                reloadData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TopicConfigListViewDataModel dataModel = (TopicConfigListViewDataModel)topicConfigListViewAdapter.getItem(position);

        boolean isSuccess = CiotpfRosNode.getInstance().setManualTopicConfig(appName, isSubscription, !dataModel.isAlreadySubscribed, dataModel.topicName, manualTopicDataModel.name);
        if(isSuccess) {
            dataModel.isAlreadySubscribed = !dataModel.isAlreadySubscribed;
            topicConfigListViewAdapter.notifyDataSetChanged();
        } else {
            Toast.makeText(this, "Some error happened", Toast.LENGTH_LONG).show();
        }

    }
}