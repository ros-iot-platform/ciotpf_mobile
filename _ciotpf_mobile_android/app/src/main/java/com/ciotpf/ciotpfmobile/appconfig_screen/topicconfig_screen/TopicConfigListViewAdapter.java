package com.ciotpf.ciotpfmobile.appconfig_screen.topicconfig_screen;

import android.content.Context;
import android.graphics.drawable.Icon;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciotpf.ciotpfmobile.R;

import java.util.List;

public class TopicConfigListViewAdapter extends ArrayAdapter<TopicConfigListViewDataModel> {

    private LayoutInflater layoutInflater;

    public TopicConfigListViewAdapter(Context context, int resource, List<TopicConfigListViewDataModel> objects) {
        super(context, resource, objects);
        this.layoutInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
        );

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(
                    R.layout.topic_config_list_view_item,
                    parent,
                    false
            );
        }

        TopicConfigListViewDataModel dataModel = (TopicConfigListViewDataModel)getItem(position);
        Log.e("AAAA", dataModel.topicName);

        TextView tv = convertView.findViewById(R.id.topic_config_list_view_item_tv);
        ImageView checkImageView = convertView.findViewById(R.id.topic_config_list_view_item_check_image_view);

        if(dataModel.isAlreadySubscribed) {
            checkImageView.setImageResource(android.R.drawable.checkbox_on_background);
        } else {
            checkImageView.setImageResource(android.R.drawable.checkbox_off_background);
        }
        tv.setText(dataModel.topicName);
        return convertView;
    }
}
