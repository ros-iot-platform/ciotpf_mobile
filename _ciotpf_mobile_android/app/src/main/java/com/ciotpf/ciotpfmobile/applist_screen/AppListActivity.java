package com.ciotpf.ciotpfmobile.applist_screen;


import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ciotpf.ciotpfmobile.R;
import com.ciotpf.ciotpfmobile.appconfig_screen.AppConfigActivity;
import com.ciotpf.CiotpfRosNode;

import org.ros2.rcljava.publisher.Publisher;

import java.util.ArrayList;

public class AppListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView applistListView;

    private AppListListViewAdapter appListListViewAdapter;
    Publisher pub;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("App List");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        this.applistListView = (ListView)findViewById(R.id.topic_config_list_view);
        this.applistListView.setOnItemClickListener(this);


        //pub = getInstance().createPublisher(CiotpfStringArray.class, "/teststringarray");

        ArrayList<String> appNames = new ArrayList<>();
        appListListViewAdapter = new AppListListViewAdapter(this, 0, appNames);
        applistListView.setAdapter(appListListViewAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();

        reloadData();
    }

    private void reloadData() {
        ArrayList<String> appNames = CiotpfRosNode.getInstance().getAllAppName();

        appListListViewAdapter.clear();
        appListListViewAdapter.addAll(appNames);
        appListListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
            case R.id.reload:
                reloadData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        /*
        CiotpfStringArray strArray = new CiotpfStringArray();

        String[] tmp = new String[] {"a", "b"};
        strArray.setData(tmp);
        pub.publish(strArray);
         */

        String appName = this.appListListViewAdapter.getItem(position);
        //String appName = this.appListListViewAdapter.getItem(position);
        //appName = appName + "_app_node";
        //AppNodeInfoDataModel dm = CiotpfRosNode.getInstance().getAppInfo(appName);

        AppConfigActivity.startActivity(this, appName);

        //og.d("AAA", dm.toString());
    }
}