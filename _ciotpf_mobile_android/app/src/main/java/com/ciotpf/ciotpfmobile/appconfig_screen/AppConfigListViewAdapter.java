package com.ciotpf.ciotpfmobile.appconfig_screen;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ciotpf.ciotpfmobile.R;
import com.ciotpf.ciotpfmobile.utils.AppNodeInfoDataModel;
import com.ciotpf.ciotpfmobile.utils.ManualTopicDataModel;

import java.util.List;

enum SectionType {
    Subscriber,
    Publisher,
    HeaderSubscription,
    HeaderPublisher
}

public class AppConfigListViewAdapter extends BaseAdapter {


    private AppNodeInfoDataModel appNodeInfoDataModel;

    private LayoutInflater layoutInflater;


    public AppConfigListViewAdapter(Context context, AppNodeInfoDataModel appNodeInfoDataModel) {
        super();
        this.layoutInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
        );
        this.appNodeInfoDataModel = appNodeInfoDataModel;

    }


    @Override
    public int getCount() {
        return this.appNodeInfoDataModel.subscriptions.length + this.appNodeInfoDataModel.publishers.length + 2;
    }

    @Override
    public Object getItem(int position) {
        SectionType sectionType = this.getSectionType(position);

        if(sectionType == SectionType.HeaderSubscription) {
            return "Subscription";
        }
        if(sectionType == SectionType.Subscriber) {
            return this.appNodeInfoDataModel.subscriptions[position - 1];
        }
        if(sectionType == SectionType.HeaderPublisher) {
            return "Publisher";
        }

        return this.appNodeInfoDataModel.publishers[position - this.appNodeInfoDataModel.subscriptions.length - 2];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    public void replaceData(AppNodeInfoDataModel appNodeInfoDataModel) {
        this.appNodeInfoDataModel = appNodeInfoDataModel;
    }

    @Override
    public boolean isEnabled(int position) {
        SectionType sectionType = getSectionType(position);

        if(sectionType == SectionType.HeaderPublisher || sectionType == SectionType.HeaderSubscription) {

           return false;

        } else {
           return true;
        }
    }

    public boolean isSubscribe(int position) throws Exception {
        SectionType sectionType = getSectionType(position);
        if(sectionType == SectionType.Publisher) {

            return false;

        } else if(sectionType == SectionType.Subscriber) {
            return true;
        }

        throw new Exception();
    }

    private SectionType getSectionType(int position) {
        if(position == 0) {
            return SectionType.HeaderSubscription;
        }

        if(position < this.appNodeInfoDataModel.subscriptions.length + 1) {
            return SectionType.Subscriber;
        }
        if(position == this.appNodeInfoDataModel.subscriptions.length + 1) {
            return  SectionType.HeaderPublisher;
        }

        return SectionType.Publisher;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SectionType type = this.getSectionType(position);

        if (convertView == null) {

            if(type == SectionType.HeaderPublisher || type == SectionType.HeaderSubscription) {
                convertView = layoutInflater.inflate(R.layout.app_list_list_view_item,parent, false);
                convertView.setBackgroundColor(Color.GRAY);
            } else {
                convertView = layoutInflater.inflate(
                        R.layout.app_list_list_view_item,
                        parent,
                        false
                );
            }

        }

        if(type == SectionType.HeaderPublisher || type == SectionType.HeaderSubscription) {

            String text = (String) getItem(position);

            TextView tv = convertView.findViewById(R.id.app_list_list_view_item_tv);
            tv.setText(text);

        } else {
            ManualTopicDataModel manualTopicDataModel = (ManualTopicDataModel)getItem(position);


            TextView tv = convertView.findViewById(R.id.app_list_list_view_item_tv);
            tv.setText(manualTopicDataModel.name);
        }

        return convertView;
    }
}
