package com.ciotpf.ciotpfmobile.utils;

import java.io.Serializable;
import java.util.Arrays;

public class AppNodeInfoDataModel implements Serializable  {

    public ManualTopicDataModel[] subscriptions;
    public ManualTopicDataModel[] publishers;

    public AppNodeInfoDataModel(ManualTopicDataModel[] subscriptions, ManualTopicDataModel[] publishers) {
        this.subscriptions = subscriptions;
        this.publishers = publishers;
    }

    public String toString() {
        String returnVal = "subscriptions \n";
        String tmp = "";
        for(ManualTopicDataModel sub: this.subscriptions) {
            tmp = "";
            tmp = tmp + sub.name;

            for(String t: sub.topic_names) {
                tmp = tmp + "  " + t;
            }
            tmp = tmp + "\n";
        }

        returnVal = returnVal + tmp + "\npublishers \n";

        for(ManualTopicDataModel sub: this.publishers) {
            tmp = "";
            tmp = tmp + sub.name;

            for(String t: sub.topic_names) {
                tmp = tmp + "  " + t;
            }
        }
        returnVal = returnVal + tmp;


        return returnVal;
    }
}
