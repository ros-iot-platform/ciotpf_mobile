package com.ciotpf.ciotpfmobile.appconfig_screen.topicconfig_screen;

public class TopicConfigListViewDataModel {

    public String topicName;
    public boolean isAlreadySubscribed;

    public TopicConfigListViewDataModel(String topicName, boolean isAlreadySubscribed) {
        this.topicName = topicName;
        this.isAlreadySubscribed = isAlreadySubscribed;
    }
}
