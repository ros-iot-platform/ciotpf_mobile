package com.ciotpf.ciotpfmobile.topiclist_screen;

import android.app.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import com.ciotpf.ciotpfmobile.R;
import com.ciotpf.CiotpfRosNode;
import com.ciotpf.ciotpfmobile.utils.TopicDataModel;
import android.widget.AdapterView;
import android.widget.Toast;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.lang.reflect.Array;
import java.util.ArrayList;
import com.ciotpf.ciotpfmobile.publish_topic_screen.PublishTopicActivity;

public class TopicListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener  {

    private ListView topicListView;
    private TopicListViewAdapter topicListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("TOPIC LIST!!!!!");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        this.topicListView = (ListView)findViewById(R.id.topicListView);
        this.topicListView.setOnItemClickListener(this);


        ArrayList<TopicDataModel> topics = new ArrayList<>();

        // Adapter - ArrayAdapter
        topicListViewAdapter = new TopicListViewAdapter(this, 0, topics);
        // ListViewに表示
        this.topicListView.setAdapter(topicListViewAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        reloadData();
    }

    private void reloadData() {
        ArrayList<TopicDataModel> topics = CiotpfRosNode.getInstance().getTopicsByWildCard("/ciotpf/app/*");

        topicListViewAdapter.clear();
        topicListViewAdapter.addAll(topics);
        topicListViewAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
            case R.id.reload:
                reloadData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

        TopicDataModel dataModel = this.topicListViewAdapter.getItem(position);

        PublishTopicActivity.startActivity(this, dataModel);
    }


}