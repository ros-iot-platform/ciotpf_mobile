package com.ciotpf.ciotpfmobile.utils;

import java.io.Serializable;

public class TopicDataModel implements Serializable {

    public String topicName;
    public String topicType;
    public TopicDataModel(String[] topic) {
        if(topic.length!=2) {
            throw new IllegalArgumentException("length should be 2");
        }


        this.topicName = topic[0];
        this.topicType = topic[1];
    }

}
