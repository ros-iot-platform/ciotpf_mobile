package com.ciotpf.ciotpfmobile.applist_screen;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.ciotpf.ciotpfmobile.R;
import com.ciotpf.ciotpfmobile.utils.TopicDataModel;

import java.util.List;

public class AppListListViewAdapter extends ArrayAdapter<String> {
    private LayoutInflater layoutInflater;

    public AppListListViewAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.layoutInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
        );

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(
                    R.layout.app_list_list_view_item,
                    parent,
                    false
            );
        }

        String appName = (String)getItem(position);

        TextView tv = convertView.findViewById(R.id.app_list_list_view_item_tv);

        tv.setText(appName);
        return convertView;
    }
}
