package com.ciotpf.ciotpfmobile.utils;

import java.io.Serializable;

public class ManualTopicDataModel implements Serializable {

    public String name;
    public String topic_type;
    public String[] topic_names;

    public ManualTopicDataModel(String name, String topic_type, String[] topic_names) {
        this.name = name;
        this.topic_type = topic_type;
        this.topic_names = topic_names;
    }
}
